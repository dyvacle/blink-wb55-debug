/**
  ******************************************************************************
  * @file           : debug.c
  * @brief          : debug tools
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
#include "debug.h"
#include "printf.h"

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

extern UART_HandleTypeDef DEBUG_HANDLER; // serial monitor

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

// redefines Error_Handler

void _Error_Handler(char * file, int line)
{
	char dbg_txt[TXT_TAG_SIZE+TXT_OUT_SIZE];

	HAL_GPIO_WritePin(R_LED_GPIO_Port, R_LED_Pin, SET);
    snprintf(dbg_txt, TXT_TAG_SIZE+TXT_OUT_SIZE, "Error reported... File %s on line %d\n\r", file, line);
    HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t*) dbg_txt, strlen(dbg_txt), strlen(dbg_txt)*TIMEOUT_OUT);
}

// redefines output input standard io

// extern HAL_StatusTypeDef HAL_UART_Transmit(
// 	UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

// extern HAL_StatusTypeDef HAL_UART_Receive(
// 	UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

#ifdef __GNUC__

int __io_putchar(int ch)
{
   uint8_t ch8 = ch;
   HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *)&ch8, 1, TIMEOUT_OUT);
   return ch;
}

int __io_getchar()
{
   uint8_t ch8;
   HAL_UART_Receive(&DEBUG_HANDLER, &ch8, 1, TIMEOUT_IN);
   return (int)ch8;
}

#else

int fputc(int ch, FILE *f)
{
  HAL_UART_Transmit(&DEBUG_HANDLER, (uint8_t *)&ch, 1, TIMEOUT_OUT);
  return ch;
}

int __io_getchar()
{
   uint8_t ch8;
   HAL_UART_Receive(&DEBUG_HANDLER, &ch8, 1, TIMEOUT_IN);
   return (int)ch8;
}

#endif /* __GNUC__ */


