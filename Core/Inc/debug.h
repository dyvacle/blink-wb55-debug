/**
  ******************************************************************************
  * @file           : debug.h
  * @brief          : Header for debug.c file.
  *                   This file contains the debug tools.
  *
  *                   Define DEBUG or not to have trace or not
  *                   See DEBUG_UART : define LP UART 1 on PA2 tx & PA3 rx
  *
  *                   use debug macros for trace
  *                   dbg_printf with ticks
  *                   dbg_log with file + line
  *
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEBUG_H
#define __DEBUG_H

/* Private includes ----------------------------------------------------------*/

// HAL
#include "stm32wbxx_hal.h"
#include "stm32wbxx_ll_system.h"

// project
#include "printf.h"
#include "main.h"

// use common libs
#include <stdbool.h>	// bool
#include <string.h>		// strlen...

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

#define DEBUG_HANDLER		huart1

#ifndef DEBUG /* to avoid warings */
#define DEBUG /*to be defined or to be in comment*/
#endif /* to avoid warings */

#define TIMEOUT_IN		10   /* 10 ms short time out for char input */
#define TIMEOUT_OUT		10   /* 10 ms time out for each char of text output */
#define TXT_IN_SIZE		255  /* max size buffer in */
#define TXT_TAG_SIZE	55  /* max size tag */
#define TXT_OUT_SIZE	200  /* max size buffer out */
#define INPUT_EOT		'\r' /* Carriage return */

/* Exported macro ------------------------------------------------------------*/

// debug strings
char dbg_tag[TXT_TAG_SIZE];
char dbg_msg[TXT_OUT_SIZE];

#ifdef DEBUG /* DeBUGGING MODE */

#define TRACE true

/* macro to trace debug on serial monitor  with ticks */
#define dbg_printf(...) { \
	sprintf(dbg_tag, "<%lu>", HAL_GetTick());  /* ticks tag */ \
	sprintf(dbg_msg, __VA_ARGS__); /* args */ \
	printf("\r\n%s %s", dbg_tag, dbg_msg); /* concat */ \
} /* end debug macro */

/* to have file name without path */
// #define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

/* macro to trace debug on serial monitor with file & line number */
#define dbg_log(...) { \
	sprintf(dbg_tag, "%s [%d]", __FILENAME__, __LINE__);  /* file line tag */ \
	sprintf(dbg_msg, __VA_ARGS__); /* args */ \
	printf("\r\n%s %s", dbg_tag, dbg_msg); /* concat */ \
} /* end debug macro */

#else /* NO DEBUG */

#define TRACE false

/* macro are NOP */
#define dbg_printf(...) {}
#define dbg_log(...) {}

#endif /* DEBUG */

/* Exported functions prototypes ---------------------------------------------*/

#define GET_MACRO( _0, _1, NAME, ... ) NAME
#define Error_Handler(...) GET_MACRO( _0, ##__VA_ARGS__, Error_Handler1, Error_Handler0 )()
#define Error_Handler0() _Error_Handler( __FILE__, __LINE__ )
#define Error_Handler1(unused) _Error_Handler( char * file, int line )
void 	_Error_Handler(char *, int);

/* Private defines -----------------------------------------------------------*/

/* Redefines the weak functions :

#ifdef __GNUC__
int __io_putchar(int ch);
int __io_getchar();
#else
int fputc(int ch, FILE *f);
int __io_getchar();
#endif
*/

#endif /* __DEBUG_H */

